
### General
---

General-purpose CRUD generator for Symfony-enabled web applications. Generates Doctrine documents and related stuff (Repositories, managers, services and rest CRUD controllers)

### Features & Capabilities
---
- This bundle provides the following commands:

    * `ats:generator:generate:document` : Used to generate Doctrine ODM Documents, Managers & CRUD Services
    * `ats:generator:generate:rest` : Used to generate Symfony REST-compliant controllers. Controller logic is divided between Services, Managers and Repositories, as dictated by [ats/core-bundle](https://gitlab.ats-digital.com/ats/bundles/core-bundle) guidelines.
    * `ats:generator:generate:fake` : Used to generate fake data (Based on the [Faker library](https://github.com/fzaninotto/Faker))

### Setup
---

*N.B. : This bundle is designed to be used solely in a development environment*

1. Install the bundle in your application
```sh
$ php composer require ats/generator-bundle --dev
```
2. Register the bundle in the AppKernel
```php
<?php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // production-enabled bundles...
        ];

        if ('dev' === $this->getEnvironment()) {
            // ...
            $bundles[] = new ATS\GeneratorBundle\ATSGeneratorBundle();
        }
    }
    // ...
}
```
3. You're all set.

### Configuration
- No configuration required.

### Roadmap
* Doctrine ORM support
* Multi-document generation