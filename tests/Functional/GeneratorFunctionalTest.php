<?php

namespace ATS\GeneratorBundle\Tests\Functional;

use ATS\GeneratorBundle\ATSGeneratorBundle;
use ATS\GeneratorBundle\Document\Product;
use ATS\GeneratorBundle\Generator\DocumentGenerator;
use ATS\GeneratorBundle\Generator\RestControllerGenerator;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GeneratorFunctionalTest extends WebTestCase
{

    const DOCUMENT_CLASS = 'ATSGeneratorBundle:Product';
    private $container;

    public function setUp()
    {
        $kernel = static::bootKernel();
        $this->container = $kernel->getContainer();
        $this->managerRegistry = $this->container->get('doctrine_mongodb');
        $this->documentManager = $this->managerRegistry->getManager();
        $this->serializer = $this->container->get('jms_serializer');

        $this->client = self::createClient([]);
    }

    public function testGenerateDocumentDefinition()
    {
        $generatorBundle = new ATSGeneratorBundle();

        $generator = new DocumentGenerator($this->container);
        $generator->setSkeletonDirs(
            $this
                ->container
                ->getParameter('kernel.root_dir') . '/../src/Resources/skeleton'
        );
        $generator->generate(
            $generatorBundle,
            'Product',
            array(
                'name' => [
                    'fieldName' => "name",
                    'isNullable' => false,
                    'defaultValue' => null,
                    'type' => "string",
                    'mongoType' => "string",
                    'jmsType' => "string",
                    'basicType' => true,
                    'associationType' => "",
                    'associationMany' => false,
                ],
            ),
            array(),
            false
        );

        $generator = new RestControllerGenerator($this->container->get('filesystem'), $this->container);

        $generator->setSkeletonDirs(
            $this
                ->container
                ->getParameter('kernel.root_dir') . '/../src/Resources/skeleton'
        );
        $generator->generate(
            $generatorBundle,
            'Product'
        );
        $this->assertTrue(class_exists(Product::class));
    }

    public function testGenerateDocumentInstances()
    {
        $entity = self::DOCUMENT_CLASS;
        $this->documentManager->getDocumentCollection(self::DOCUMENT_CLASS)->remove([]);
        $count = 50;
        $generator = Factory::create();
        $populator = new Populator($generator, $this->documentManager);
        $populator->addEntity($entity, $count);
        $populator->execute();
        $dbCount = count(
            $this
                ->documentManager
                ->getRepository(self::DOCUMENT_CLASS)
                ->findAll()
        );
        $this->assertEquals($count, $dbCount);
    }

    public function testListRoute()
    {
        $this->client->request('GET', '/product/list');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(50, $content);
    }

    public function testGetRoute()
    {
        $product = $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findOneBy([]);

        $this->client->request('GET', '/product/' . $product->getId() . '/get');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $wsDocument = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($wsDocument['id'], $product->getId());
    }

    public function testPaginateRoute()
    {
        $this->client->request('GET', '/product/paginate');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(20, $content);
    }

    public function testNewRoute()
    {
        $name = 'product_' . uniqid();

        $productClass = $this
            ->documentManager
            ->getRepository(self::DOCUMENT_CLASS)
            ->getClassName();

        $newProduct = (new $productClass)->setName($name);

        $currentCount = count($this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll());
        $json = $this->serializer->serialize($newProduct, 'json');
        $response = $this->client->request(
            'POST',
            '/product/new',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );

        $productId = json_decode($this->client->getResponse()->getContent());

        $afterInsertCount = count($this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll());
        $newProduct = $this->documentManager->getRepository(self::DOCUMENT_CLASS)->find($productId);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertEquals($currentCount + 1, $afterInsertCount);
        $this->assertNotNull($newProduct);
    }

    public function testUpdateRoute()
    {
        $all = $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll();

        $currentCount = count($all);
        $product = $all[0];
        $name = 'product_' . uniqid();
        $json = $this->serializer->serialize($product->setName($name), 'json');
        $this->client->request(
            'PUT',
            '/product/' . $product->getId() . '/update',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );

        $updatedProduct = $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findOneBy(['name' => $name]);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertCount($currentCount, $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll());
        $this->assertNotNull($updatedProduct);
    }

    public function testDeleteRoute()
    {
        $all = $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll();

        $currentCount = count($all);
        $product = $all[0];
        $this->client->request('DELETE', '/product/' . $product->getId() . '/delete');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertCount($currentCount - 1, $this->documentManager->getRepository(self::DOCUMENT_CLASS)->findAll());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->documentManager->close();
        $this->documentManager = null; // avoid memory leaks
    }
}
