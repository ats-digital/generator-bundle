<?php

namespace ATS\GeneratorBundle\Tests\Unit\Command;

use ATS\GeneratorBundle\Command\GenerateDocumentCommand;
use ATS\GeneratorBundle\Command\GenerateFakeDataCommand;
use ATS\GeneratorBundle\Command\GenerateRestControllerCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GenerateAndFakeCommandTest extends KernelTestCase
{

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->documentManager = $kernel->getContainer()
        ->get('doctrine_mongodb')
        ->getManager();

        $this->documentManager->getSchemaManager()->dropDatabases();
    }

    public function testGenerateDocument()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateDocumentCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        // generate Foo Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Foo',
                0,
                'name', 'string', 'yes', 'John doe',
                'name', // ensures fool-proof input
                'age', 'int', 'no', '42',
                'revenue', 'float', 'yes', '1000.0',
                'active', 'bool', 'no', 'true',
                'createdAt', 'datetime', 'yes',
                'extraInfo', 'array', 'no', '["one","two","three"]',
                'somethingNullable', 'string', 'yes', 'null',
                'somethingNotNullable', 'string', 'no', 'lorem ipsum dolor',
                '',
                'name', '',
                '',
                'yes',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);

        // generate Baz Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Baz',
                0,
                'name', 'string', 'yes', '',
                'age', 'int', 'no', '',
                'revenue', 'float', 'yes', '',
                'active', 'bool', 'no', '',
                'createdAt', 'datetime', 'yes',
                'extraInfo', 'array', 'no', '',
                '',
                'name', '',
                '',
                'yes',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);

        // generate Bar Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Bar',
                1,
                'name', 'string', 'yes', '',
                'age', 'int', 'no', '',
                'revenue', 'float', 'yes', '',
                'active', 'bool', 'no', '',
                'createdAt', 'datetime', 'yes',
                'extraInfo', 'array', 'no', '',
                '',
                'name', '',
                '',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        // generate Qux Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Qux',
                1,
                'name', 'string', 'yes', '',
                'age', 'int', 'no', '',
                'revenue', 'float', 'yes', '',
                'active', 'bool', 'no', '',
                'createdAt', 'datetime', 'yes',
                'extraInfo', 'array', 'no', '',
                '',
                'name', '',
                '',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);

        // generate Quux Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Quux',
                0,
                'name', 'string', 'yes', '',
                'age', 'int', 'no', '',
                'revenue', 'float', 'yes', '',
                'active', 'bool', 'no', '',
                'createdAt', 'datetime', 'yes',
                'extraInfo', 'array', 'no', '',
                'baz', 'ATSGeneratorBundle:Baz', 'ReferenceOne', 'yes',
                'foos', 'ATSGeneratorBundle:Foo', 'ReferenceMany', 'no',
                'bar', 'ATSGeneratorBundle:Bar', 'EmbedOne', 'yes',
                'quxes', 'ATSGeneratorBundle:Qux', 'EmbedMany', 'no',
                '',
                'name', '',
                '',
                'yes',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);
    }

    public function testAbortGenerateDocumentCommand()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateDocumentCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        // generate Foo Document
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Book',
                0,
                'name','string', 'yes', '',
                'price','float', 'no', '',
                'createdAt','datetime', 'yes',
                '',
                'name', '',
                '',
                'no',
                'no',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(1, $returnCode);
    }

    public function testRefBypassFailure()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateFakeDataCommand())
        );

        $command = $application->find('ats:generator:generate:fake');
        $commandTester = new CommandTester($command);

        // Create complex object without existing refs in DB to ensure --ref-bypass failure

        $this->expectException(\RuntimeException::class);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Quux',
                '--ref-bypass' => true,
            ]
        );
    }

    public function testFake()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateFakeDataCommand())
        );

        $command = $application->find('ats:generator:generate:fake');
        $commandTester = new CommandTester($command);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Baz',
            ]
        );

        $this->assertEquals(0, $returnCode);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Foo',
            ]
        );

        $this->assertEquals(0, $returnCode);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Qux',
            ]
        );

        $this->assertEquals(0, $returnCode);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Quux',
            ]
        );

        $this->assertEquals(0, $returnCode);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Quux',
            ]
        );

        $this->assertEquals(0, $returnCode);

        // Re-executes command to test --ref-bypass

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Quux',
                '--ref-bypass' => true,
                '--count' => 20,
            ]
        );

        $this->assertEquals(0, $returnCode);

        // Re-executes command to test --purge

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'entity' => 'ATSGeneratorBundle:Baz',
                '--purge' => true,
                '--count' => 20,
            ]
        );

        $this->assertEquals(0, $returnCode);
    }

    public function testInvalidControllerName()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:rest');
        $commandTester = new CommandTester($command);

        $this->expectException(\InvalidArgumentException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
                '--controller' => '__invalid__',
            ]
        );
    }

    public function testInvalidDocumentName()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        $this->expectException(\InvalidArgumentException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
                '--controller' => '__invalid__',
            ]
        );
    }


    public function testInvalidBundleName()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs(['__invalid__']);

        $this->expectException(\InvalidArgumentException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
    }


    public function testInexistantBundleName()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs([
            'FooBarBazQuxBundle:Post',
            0,
            '',
            '',
            'yes',
            'yes',
        ]);

        $this->expectException(\RuntimeException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
    }

    public function testFailIfNonInteractive()
    {

        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateDocumentCommand())
        );

        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);

        $this->expectException(\RuntimeException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
                '--document' => 'ATSGeneratorBundle:Post',
            ],
            [
                'interactive' => false
            ]
        );
    }

    public function testInteractIfEmptyControllerName()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:rest');
        $commandTester = new CommandTester($command);


        $this->assertEquals(0, 0);
    }

    public function testFailIfNonNullTypeHint()
    {
        $baz = new \ATS\GeneratorBundle\Document\Baz();
        $this->expectException(\TypeError::class);
        $baz->setAge(null);
    }

    public function testFailIfPrimitiveTypeMismatch()
    {
        $baz = new \ATS\GeneratorBundle\Document\Baz();
        $this->expectException(\TypeError::class);
        $baz->setAge("__invalid__");
    }

    public function testSuccessIfNullTypeHint()
    {
        $quux = new \ATS\GeneratorBundle\Document\Quux();
        $quux->setBar(null);
        $this->assertNull($quux->getBar());
    }

    public function testValidDefaultValues()
    {
        $foo = new \ATS\GeneratorBundle\Document\Foo();
        $this->assertEquals($foo->getName(), 'John doe');
        $this->assertEquals($foo->getAge(), 42);
        $this->assertEquals($foo->getRevenue(), 1000.0);
        $this->assertEquals($foo->isActive(), true);
        $this->assertEquals($foo->getExtraInfo(), ["one", "two", "three"]);
    }
}
