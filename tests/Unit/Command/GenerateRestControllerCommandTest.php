<?php

namespace ATS\GeneratorBundle\Tests\Unit\Command;

use ATS\GeneratorBundle\Command\GenerateRestControllerCommand;
use ATS\GeneratorBundle\Command\GenerateDocumentCommand;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * GenerateRestControllerCommandTest
 *
 * @author Mohamed BEN ABDA <mbenabda@ats-digital.com>
 */
class GenerateRestControllerCommandTest extends KernelTestCase
{
    /**
     * Test Generate routing
     *
     * @uses ATS\GeneratorBundle\Command\GenerateRestControllerCommand
     * @uses ATS\GeneratorBundle\Command\GenerateDocumentCommand
     */
    public function testGenerateRouting()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        // Generate Foo Document without RestController
        $application->add(
            (new GenerateDocumentCommand())
        );
        $command = $application->find('ats:generator:generate:document');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(
            [
                'ATSGeneratorBundle:Employee',
                0,
                'name','string', 'yes', '',
                'age','int', 'no', '',
                'revenue','float', 'yes', '',
                'active','bool', 'no', '',
                'createdAt','datetime', 'yes',
                'extraInfo','array', 'no', '',
                '',
                '',
                'no',
                'yes',
            ]
        );
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);

        // Delete routing file befor generate RestController
        $bundle = $kernel->getContainer()->get('kernel')->getBundle('ATSGeneratorBundle');
        $file = $bundle->getPath().'/Resources/config/routing.yml';
        if (file_exists($file) === true) {
            unlink($file);
        }
        $this->assertFalse(file_exists($file));

        $dir = $bundle->getPath().'/Resources/config';
        $this->assertTrue(is_dir($dir));

        // Generate RestController to the Foo Document
        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:rest');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs(['ATSGeneratorBundle:Employee']);

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        $this->assertEquals(0, $returnCode);
    }

    public function testInvalidController()
    {

        $kernel = self::bootKernel();
        $application = new Application($kernel);

        // Generate RestController to the Foo Document
        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:rest');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs(['FooBarBazQux:Employee']);

        $this->expectException(\RuntimeException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
    }

    public function testMissingController()
    {

        $kernel = self::bootKernel();
        $application = new Application($kernel);

        // Generate RestController to the Foo Document
        $application->add(
            (new GenerateRestControllerCommand())
        );

        $command = $application->find('ats:generator:generate:rest');
        $commandTester = new CommandTester($command);

        $this->expectException(\RuntimeException::class);

        $commandTester->execute(
            [
                'command' => $command->getName(),
            ],
            [
                'interactive' => false
            ]
        );
    }
}
