<?php declare (strict_types = 1);

namespace ATS\GeneratorBundle\Tests\Unit\DependencyIntection;

use ATS\GeneratorBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * ConfigurationTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test suits for getConfigTreeBuilder
     *
     * @covers ATS\GeneratorBundle\DependencyInjection\Configuration::getConfigTreeBuilder
     */
    public function testGgetConfigTreeBuilder()
    {
        $configuration = new Configuration();
        $this->assertInstanceOf(TreeBuilder::class, $configuration->getConfigTreeBuilder());
    }
}
