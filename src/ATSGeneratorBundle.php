<?php declare(strict_types=1);

namespace ATS\GeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ATSGeneratorBundle extends Bundle
{
}
