<?php declare (strict_types = 1);

namespace ATS\GeneratorBundle\Command;

use ATS\CoreBundle\Service\Util\AString;
use ATS\GeneratorBundle\Generator\DocumentGenerator;
use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Sensio\Bundle\GeneratorBundle\Command\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;

class GenerateDocumentCommand extends GeneratorCommand
{

    const DOCUMENT_MAPPING_EMBEDDED = 'embedded';
    const DOCUMENT_MAPPING_REGULAR = 'regular';

    protected function configure()
    {
        $this->setName('ats:generator:generate:document')
            ->setDescription('Generates a new Doctrine document inside a bundle')
            ->addOption(
                'document',
                null,
                InputOption::VALUE_REQUIRED,
                'The document class name to initialize (shortcut notation)'
            )
            ->addOption(
                'document-mapping',
                null,
                InputOption::VALUE_REQUIRED,
                'Whether document should be embedded or regular'
            )
            ->addOption(
                'fields',
                null,
                InputOption::VALUE_REQUIRED,
                'The fields to create with the new document'
            )
            ->addOption(
                'indexes',
                null,
                InputOption::VALUE_REQUIRED,
                'The indexes to create with the new document'
            )
            ->addOption(
                'with-controller',
                null,
                InputOption::VALUE_NONE,
                'Whether to generate the associated Rest Controller or not'
            )
            ->addOption(
                'no-strict',
                null,
                InputOption::VALUE_NONE,
                'Strict Check'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var string
         */
        $documentName = $input->getOption('document');
        /**
         * @var array
         */
        $fields = $input->getOption('fields');
        /**
         * @var array
         */
        $indexes = $input->getOption('indexes');

        list($bundleName, $document) = $this->parseShortcutNotation($documentName);
        $bundle = null;

        if (is_string($bundleName) === true) {
            try {
                $bundle = $this->getContainer()->get('kernel')->getBundle($bundleName);
            } catch (\Exception $e) {
                throw new \RuntimeException(sprintf('Bundle "%s" does not exist.', $bundleName));
            }
        }

        $questionHelper = $this->getQuestionHelper();

        if ($input->isInteractive() === true) {
            if ($this->getGenerator($bundle)->didGenerate($bundle, $document) === true) {
                $output->writeln('<comment>Document already exists, proceed at your own risk</comment>');
            }

            $confirmationQuestion = new ConfirmationQuestion(
                $questionHelper->getQuestion('Do you confirm generation', 'yes', '?'),
                true
            );

            $output->writeln($this->previewGeneration($fields));

            if (false === $questionHelper->ask($input, $output, $confirmationQuestion)) {
                $output->writeln('<error>Command aborted</error>');
                return 1;
            }
        } else {
            throw new \RuntimeException("Non-interactive mode not implemented yet");
        }

        $questionHelper->writeSection($output, 'Document generation');

        /** @var DocumentGenerator $generator */
        $generator = $this->getGenerator($bundle);
        $generator->setSkeletonDirs(__DIR__ . '/../Resources/skeleton');
        $generator->generate(
            $bundle,
            $document,
            $fields,
            $indexes,
            $this->isEmbedded($input)
        );

        $output->writeln('Generating the Document code: <info>OK</info>');

        $withController = $input->getOption('with-controller');

        if ($withController === true) {
            $command = $this->getApplication()->find('ats:generator:generate:rest');

            $arguments = array(
                '--controller' => $bundle->getName() . ':' . $document,
            );

            $returnCode = $command->run(new ArrayInput($arguments), $output);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();

        $noStrict = $input->getOption('no-strict');

        $bundleNames = array_keys($this->getContainer()->get('kernel')->getBundles());

        $question = new Question(
            $questionHelper->getQuestion(
                'The Document shortcut name',
                $input->getOption('document')
            ),
            $input->getOption('document')
        );

        $question->setAutocompleterValues($bundleNames);

        $document = $questionHelper->ask($input, $output, $question);
        list($bundle, $document) = $this->parseShortcutNotation($document);
        $document = ucfirst($document);
        $input->setOption('document', $bundle . ':' . $document);

        // Document fields

        $entities = [];

        foreach ($bundleNames as $bundleName) {
            /** @var BundleInterface $bundle */
            $bundle = $this->getContainer()->get('kernel')->getBundle($bundleName);
            $finder = new Finder();
            if (is_dir(__DIR__ . '/../../' . $bundle->getNamespace() . '/Document') === true) {
                $finder->files()->in(__DIR__ . '/../../' . $bundle->getNamespace() . '/Document');
                foreach ($finder as $file) {
                    $className = explode('.', $file->getFilename())[0];
                    $entities[] = "$bundleName:$className";
                }
            }
        }

        $documentMappingQuestion = new ChoiceQuestion(
            'Please select document mapping',
            [self::DOCUMENT_MAPPING_REGULAR, self::DOCUMENT_MAPPING_EMBEDDED],
            0
        );

        $input->setOption(
            'document-mapping',
            $questionHelper->ask($input, $output, $documentMappingQuestion)
        );

        /**
         * @var array
         */

        $fields = [];

        while (true) {
            $output->writeln('');

            if (null === ($name = $this->askForFieldName($input, $output, $questionHelper, $fields))) {
                break;
            }

            $default = $this->inferPhpType((new AString($name)));

            $question = new Question(
                $questionHelper->getQuestion('Field type', $default),
                $default
            );

            $question->setAutocompleterValues(array_merge($entities, $this->getBasicTypes()));

            $type = $questionHelper->ask($input, $output, $question);

            if ($type === "datetime") {
                $type = "\\DateTime";
            }

            if (false === in_array($type, $this->getBasicTypes())) {
                list($bundle, $document) = $this->parseShortcutNotation($type);

                if (is_string($bundle) === true) {
                    try {
                        /** @var BundleInterface $bundle */
                        $bundle = $this->getContainer()->get('kernel')->getBundle($bundle);
                    } catch (\Exception $e) {
                        $output->writeln(sprintf('<bg=red>Bundle "%s" does not exist.</>', $bundle->getName()));
                        continue;
                    }
                }

                $type = $bundle->getNamespace() . '\\Document\\' . $document;

                if ($noStrict === false) {
                    try {
                        $class = new \ReflectionClass($type);
                    } catch (\ReflectionException $e) {
                        $output->writeln(sprintf('<bg=red>Class "%s" does not exist.</>', $type));
                        continue;
                    }
                }

                $default = (new AString($name))->endsWith('s') === true ? 'ReferenceMany' : 'ReferenceOne';

                $question = new Question(
                    $questionHelper->getQuestion('Association type', $default),
                    $default
                );
                $question->setAutocompleterValues(['ReferenceOne', 'ReferenceMany', 'EmbedOne', 'EmbedMany']);
                $associationType = $questionHelper->ask($input, $output, $question);
                $associationMany = in_array($associationType, ['ReferenceMany', 'EmbedMany']);

                if (false === in_array($associationType, ['ReferenceOne', 'ReferenceMany', 'EmbedOne', 'EmbedMany'])) {
                    $output->writeln(sprintf('<bg=red>Bad association "%s".</>', $associationType));
                    continue;
                }

                $isBasicType = false;
            } else {
                $isBasicType = true;
                $associationType = '';
                $associationMany = false;
            }


            $question = new ConfirmationQuestion(
                $questionHelper->getQuestion('Is field nullable ?', 'yes')
            );
            $question->setAutocompleterValues(['yes', 'no']);

            $isNullable = $questionHelper->ask($input, $output, $question);
            $defaultValue = null;

            if ($this->shouldPromptDefaultValue($type) === true) {
                $isValid = false;

                while ($isValid === false) {
                    $question = new Question(
                        $questionHelper->getQuestion("What default value should field have ? (leave empty for none, '' for empty string)", '')
                    );

                    $defaultValue = $questionHelper->ask($input, $output, $question);
                    $isValid = $this->validateDefaultValue($defaultValue, $type, $isNullable);

                    if ($isValid === false) {
                        $output->writeln('<error>Invalid default value</error>');
                    }
                }
            }

            $fields[$name] = array(
                'fieldName' => $name,
                'type' => $type,
                'isNullable' => $isNullable,
                'defaultValue' => $defaultValue,
                'mongoType' => $this->resolveMongoType($type),
                'jmsType' => $this->resolveSerializerType($type),
                'basicType' => $isBasicType,
                'associationType' => $associationType,
                'associationMany' => $associationMany,
            );
        }

        $input->setOption('fields', $fields);

        $output->writeln('');

        /**
         * @var array
         */
        $indexes = [];

        while (true) {
            $output->writeln('');

            if (null === ($name = $this->askForIndex($input, $output, $questionHelper, $indexes))) {
                break;
            }

            if (false === isset($fields[$name])) {
                $output->writeln(sprintf('<bg=red>Field "%s" doesn\'t exists!</>', $name));
                continue;
            }

            $question = new Question(
                $questionHelper->getQuestion('Index order', 'asc'),
                'asc'
            );

            $question->setAutocompleterValues(array_merge($entities, $this->getBasicTypes()));

            $order = $questionHelper->ask($input, $output, $question);

            if (false === in_array($order, ['asc', 'desc'])) {
                $output->writeln(sprintf('<bg=red>Bad order "%s".</>', $order));
                continue;
            }

            $indexes[$name] = [
                'name' => $name,
                'order' => $order
            ];
        }
        $input->setOption('indexes', $indexes);

        $output->writeln('');

        if (false === $this->isEmbedded($input)) {
            $controllerQuestion = new ConfirmationQuestion(
                $questionHelper->getQuestion(
                    'Do you want to generate a Rest Controller',
                    'yes',
                    '?'
                )
            );

            $withController = $questionHelper->ask($input, $output, $controllerQuestion);

            $input->setOption('with-controller', $withController);
        } else {
            $input->setOption('with-controller', false);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $questionHelper
     * @param array $fields
     * @return mixed
     */
    private function askForFieldName(
        InputInterface $input,
        OutputInterface $output,
        QuestionHelper $questionHelper,
        $fields
    ) {

        $question = new Question(
            $questionHelper->getQuestion('New field name (press <return> to stop adding fields)', null)
        );

        $question->setValidator(
            function ($name) use ($fields) {
                if (isset($fields[$name]) === true || 'id' === $name) {
                    throw new \InvalidArgumentException(sprintf('Field "%s" is already defined.', $name));
                }
                return $name;
            }
        );

        return $questionHelper->ask($input, $output, $question);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $questionHelper
     * @param array $indexes
     * @return mixed
     */
    private function askForIndex(
        InputInterface $input,
        OutputInterface $output,
        QuestionHelper $questionHelper,
        $indexes
    ) {
        $question = new Question(
            $questionHelper->getQuestion('New index name (press <return> to stop adding indexes)', null)
        );

        $question->setValidator(
            function ($name) use ($indexes) {
                if (isset($indexes[$name]) === true) {
                    throw new \InvalidArgumentException(sprintf('Index "%s" is already defined.', $name));
                }
                return $name;
            }
        );

        return $questionHelper->ask($input, $output, $question);
    }


    protected function createGenerator()
    {
        return new DocumentGenerator($this->getContainer());
    }

    /**
     * @param string $shortcut
     */
    public function parseShortcutNotation($shortcut)
    {
        $entity = str_replace('/', '\\', $shortcut);
        $pos = strpos($entity, ':');

        if (false === $pos) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The name must contain a : ("%s" given, expecting something like AcmeBlogBundle:Post)',
                    $entity
                )
            );
        }

        return array(substr($entity, 0, $pos), substr($entity, $pos + 1));
    }

    private function getBasicTypes()
    {
        return [
            'string',
            'int',
            'bool',
            'float',
            'datetime',
            '\\DateTime',
            'ArrayCollection',
            'array',
        ];
    }

    /**
     * @param string $phpType
     * @return string
     */
    private function resolveMongoType($phpType)
    {
        $mapping = [
            'string' => 'string',
            'integer' => 'integer',
            'int' => 'integer',
            'boolean' => 'boolean',
            'bool' => 'boolean',
            'float' => 'float',
            'datetime' => 'date',
            'DateTime' => 'date',
            "\\DateTime" => 'date',
            'array' => 'hash',
        ];

        if (array_key_exists($phpType, $mapping) === true) {
            return $mapping[$phpType];
        }

        return $phpType;
    }

    /**
     * @param string $phpType
     * @return string
     */
    private function resolveSerializerType($phpType)
    {
        $mapping = [
            'string' => 'string',
            'integer' => 'integer',
            'int' => 'integer',
            'boolean' => 'boolean',
            'bool' => 'boolean',
            'float' => 'float',
            'datetime' => 'DateTime',
            'DateTime' => 'DateTime',
            "\\DateTime" => 'DateTime',
        ];

        if (array_key_exists($phpType, $mapping) === true) {
            return $mapping[$phpType];
        }

        return $phpType;
    }

    /**
     * Infers what PHP variable type
     *
     * @return string
     */

    private function inferPhpType($str)
    {
        if ($str->endsWith('At') === true) {
            return 'datetime';
        } elseif ($str->endsWith('s') === true) {
            return 'array';
        }

        return 'string';
    }

    /**
     * @param InputInterface $input
     * @return bool
     */
    private function isEmbedded(InputInterface $input)
    {
        return $input->getOption('document-mapping') === self::DOCUMENT_MAPPING_EMBEDDED;
    }

    /**
     * @param string|null $defaultValue
     * @param string $type
     * @param bool $allowNull
     * @return bool
     */
    private function validateDefaultValue(?string $defaultValue, string $type, bool $allowNull)
    {
        // Handle unwanted default value
        if ($defaultValue === null) {
            return true;
        }

        // Handle non nullable default value
        if ($defaultValue === 'null' && $allowNull === false) {
            return false;
        }

        switch ($type) {
            case 'null': // null default value
                return true;
            case 'int':
                return ctype_digit($defaultValue);
            case 'float':
                $floatVal = floatval($defaultValue);
                return intval($floatVal) !== $floatVal;
            case 'string':
                return is_string($defaultValue);
            case 'bool':
                return in_array($defaultValue, ['true', 'false']);
            case 'array':
                return is_array(json_decode($defaultValue));
            default:
                throw new \Exception("Unhandled default value handling for type : $type");
        }
    }

    /**
     * @param string $fieldType
     * @return bool
     */
    private function shouldPromptDefaultValue($fieldType)
    {
        return in_array($fieldType, ['int','float','string', 'bool', 'array']);
    }

    /**
     * @param array $fields
     * @return string
     */

    private function previewGeneration($fields)
    {
        $result = '';
        $fieldSummary = array_map(
            function (array $field) {
                $fieldIsNullable = (bool)($field['isNullable']) === true ? 'yes' : 'no';
                return join(
                    ', ',
                    [
                        "Field name: " . $field['fieldName'],
                        "Field type: " . $field['type'],
                        "Nullable: " . $fieldIsNullable,
                    ]
                );
            },
            $fields
        );
        return join(PHP_EOL, $fieldSummary);
    }
}
