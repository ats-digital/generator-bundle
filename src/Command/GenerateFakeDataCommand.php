<?php declare (strict_types = 1);

namespace ATS\GeneratorBundle\Command;

use ATS\CoreBundle\Repository\BaseDocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateFakeDataCommand extends ContainerAwareCommand
{
    const DEFAULT_COUNT = 10;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var AnnotationReader
     */

    private $annotationReader;

    /**
     * @var InputInterface
     */

    private $input;

    /**
     * @var OutputInterface
     */

    private $output;

    protected function configure()
    {
        $this->setName('ats:generator:generate:fake')
            ->setDescription('Generates fake data for selected entity')
            ->addArgument('entity')
            ->addOption('count', null, InputOption::VALUE_REQUIRED, 'c', self::DEFAULT_COUNT)
            ->addOption('ref-bypass', null, InputOption::VALUE_NONE, 'r')
            ->addOption('purge', null, InputOption::VALUE_NONE, 'p');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->managerRegistry = $this->getContainer()->get('doctrine_mongodb');
        $this->annotationReader = new AnnotationReader();
        $this->faker = Factory::create();

        $this->input = $input;
        $this->output = $output;

        /**
         * @var string
         */

        $documentClass = $input->getArgument('entity');

        $manager = $this->managerRegistry->getManager();

        $count = intval($input->getOption('count'));
        $shouldPurge = $input->getOption('purge');

        if ($shouldPurge === true) {
            $output->writeln("Wiping all instances of <comment>$documentClass</comment> before generation");
            /**
             * @var BaseDocumentRepository
             **/
            $documentRepository = $manager->getRepository($documentClass);
            $documentRepository->deleteAll();
        }

        for ($i = 0; $i < $count; $i++) {
            $instance = $this->populateObject($documentClass);
            $this->managerRegistry->getManager()->persist($instance);
        }

        $this->managerRegistry->getManager()->flush();
        $output->writeln("Generated <info>$count</info> elements of <info>$documentClass</info>");
    }

    /**
     * @param string $documentClass
     * @return mixed
     */
    private function populateObject($documentClass)
    {
        $manager = $this->managerRegistry->getManager();
        $fullDocumentClass = $manager->getRepository($documentClass)->getClassName();

        $instance = new $fullDocumentClass;
        $properties = $this->fetchProperties($fullDocumentClass);
        $shouldRefBypass = $this->input->getOption('ref-bypass');

        foreach ($properties as $property) {
            $value = null;

            $annotations = $this->annotationReader->getPropertyAnnotations($property);
            foreach ($annotations as $annotation) {
                switch (get_class($annotation)) {
                    case ODM\Id::class:
                        break;
                    case ODM\Field::class:
                        switch ($annotation->type) {
                            case 'string':
                                $value = $this->faker->realText(50);
                                break;
                            case 'integer':
                                $value = $this->faker->randomNumber();
                                break;
                            case 'boolean':
                                $value = $this->faker->boolean();
                                break;
                            case 'float':
                                $value = $this->faker->randomFloat();
                                break;
                            case 'hash':
                                $value = $this->faker->shuffleArray();
                                break;
                            case 'date':
                                $value = $this->faker->dateTimeBetween('-1 month', '+1 month');
                                break;
                            default:
                                break;
                        }
                        break;
                    case ODM\ReferenceOne::class:
                        $targetClass = $annotation->targetDocument;
                        if ($shouldRefBypass === true) {
                            $value = $this->fetchRandom($targetClass);
                        } else {
                            $value = $this->populateObject($targetClass);
                            $manager->persist($value);
                        }
                        break;
                    case ODM\ReferenceMany::class:
                        $targetClass = $annotation->targetDocument;
                        if ($shouldRefBypass === true) {
                            $ref = $this->fetchRandom($targetClass);
                        } else {
                            $ref = $this->populateObject($annotation->targetDocument);
                            $manager->persist($ref);
                        }
                        $value = [$ref];
                        break;
                    case ODM\EmbedOne::class:
                        $value = $this->populateObject($annotation->targetDocument);
                        break;
                    case ODM\EmbedMany::class:
                        $ref = $this->populateObject($annotation->targetDocument);
                        $value = [$ref];
                        break;
                    default:
                        break;
                }
            }

            if ($value !== null) {
                $setterMethod = new \ReflectionMethod($fullDocumentClass, 'set' . ucfirst($property->name));
                $setterMethod->invoke($instance, $value);
            }
        }

        return $instance;
    }

    /**
     * @param string $targetClass
     * @return array
     */
    public function fetchProperties($targetClass)
    {
        $result = [];

        $reflectionClass = new \ReflectionClass($targetClass);

        while ($reflectionClass !== null) {
            foreach ($reflectionClass->getProperties() as $property) {
                $result[] = $property;
            }

            $parentClass = $reflectionClass->getParentClass();

            if ($parentClass !== false && $this->annotationReader->getClassAnnotation($parentClass, ODM\Document::class) !== null) {
                $reflectionClass = $parentClass;
            } else {
                $reflectionClass = null;
            }
        }

        return $result;
    }

    /**
     * @param string $targetClass
     * @return mixed
     */
    public function fetchRandom($targetClass)
    {
        $manager = $this->managerRegistry->getManager();
        $result = $manager->getRepository($targetClass)->findOneBy([]);

        if ($result === null) {
            throw new \RuntimeException(
                sprintf("Cannot bypass %s refs : not enough data", $targetClass)
            );
        }

        return $result;
    }
}
