<?php declare (strict_types = 1);

namespace ATS\GeneratorBundle\Twig;

use Doctrine\Common\Inflector\Inflector;

class ATSGeneratorExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ucfirst', array($this, 'capitalizeFirst')),
            new \Twig_SimpleFilter('lcfirst', array($this, 'lowerFirst')),
            new \Twig_SimpleFilter('camel2snake', array($this, 'toSnakeCase')),
            new \Twig_SimpleFilter('pluralize', array($this, 'pluralize')),
            new \Twig_SimpleFilter('singularize', array($this, 'singularize')),
        );
    }

    /**
     * @param string $str
     * @return string
     */
    public function capitalizeFirst($str)
    {
        return ucfirst($str);
    }

    /**
     * @param string $str
     * @return string string
     */
    public function lowerFirst($str)
    {
        return lcfirst($str);
    }

    /**
     * @param string $str
     * @return string string
     */
    public function toSnakeCase($str)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $str, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = ($match === strtoupper($match)) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @param string $str
     * @return string string
     */
    public function pluralize($str)
    {
        return Inflector::pluralize($str);
    }

    /**
     * @param string $str
     * @return string string
     */
    public function singularize($str)
    {
        return Inflector::singularize($str);
    }
}
