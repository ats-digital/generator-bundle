<?php declare (strict_types = 1);

namespace ATS\GeneratorBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Twig\Environment;

class DocumentGenerator extends Generator
{

    /**
     * @var array
     */
    protected $skeletonDirs;
    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string|array $skeletonDirs
     */
    public function setSkeletonDirs($skeletonDirs)
    {
        $this->skeletonDirs = is_array($skeletonDirs) === true ? $skeletonDirs : array($skeletonDirs);
    }

    /**
     * @return Environment
     */
    protected function getTwigEnvironment()
    {
        $twig = clone $this->container->get('twig');
        $twig->setLoader(new \Twig_Loader_Filesystem($this->skeletonDirs));

        return $twig;
    }

    /**
     * @param BundleInterface $bundle
     * @param string $document
     * @param array $fields
     * @param array $indexes
     * @param bool $isEmbedded
     */
    public function generate(
        BundleInterface $bundle,
        string $document,
        array $fields,
        array $indexes,
        bool $isEmbedded
    ) {
        $dir = $bundle->getPath();
        $documentFile = $dir . '/Document/' . ucfirst($document) . '.php';
        $repositoryFile = $dir . '/Repository/' . ucfirst($document) . 'Repository.php';
        $managerFile = $dir . '/Manager/' . ucfirst($document) . 'Manager.php';
        $serviceFile = $dir . '/Service/' . ucfirst($document) . 'Service.php';

        if (file_exists($documentFile) === true) {
            unlink($documentFile);
        }

        $usesArrayCollection = false;
        $associations = [];

        foreach ($fields as $field) {
            if ($field['basicType'] === false) {
                $associations[] = $field['type'];
            }
            if ($field['associationMany'] === true) {
                $usesArrayCollection = true;
                break;
            }
        }

        $parameters = array(
            'namespace' => $bundle->getNamespace(),
            'bundle' => $bundle->getName(),
            'document' => $document,
            'fields' => $fields,
            'indexes' => $indexes,
            'associations' => array_unique($associations),
            'usesArrayCollection' => $usesArrayCollection,
            'isEmbedded' => $isEmbedded,
        );

        $this->renderFile('document.php.twig', $documentFile, $parameters);

        if ($isEmbedded === false) {
            if (file_exists($repositoryFile) === true) {
                unlink($repositoryFile);
            }

            $this->renderFile('repository.php.twig', $repositoryFile, $parameters);

            if (file_exists($managerFile) === true) {
                unlink($managerFile);
            }

            $this->renderFile('manager.php.twig', $managerFile, $parameters);

            if (file_exists($serviceFile) === true) {
                unlink($serviceFile);
            }

            $this->renderFile('service.php.twig', $serviceFile, $parameters);
        }
    }

    /**
     * @param BundleInterface $bundle
     * @param string $document
     * @return bool
     */
    public function didGenerate(BundleInterface $bundle, string $document)
    {
        $dir = $bundle->getPath();
        $documentFile = $dir . '/Document/' . ucfirst($document) . '.php';
        return file_exists($documentFile) === true;
    }
}
